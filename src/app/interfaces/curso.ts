export interface Curso {
    id: string;
    tituloCurso: string;
    descripcionCurso: string;
    urlPrograma: string;
}
