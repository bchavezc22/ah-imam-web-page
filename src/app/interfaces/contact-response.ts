import { ControlResponse } from "./control-response";

export interface ContactResponse {
    control: ControlResponse;
    detailResponse: string;
}
