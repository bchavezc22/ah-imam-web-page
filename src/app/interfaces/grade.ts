export interface Grade {
    id: string;
    tituloCurso: string;
    descripcionCurso: string;
    Modalidad: string;
    Duración: string;
    HECSE: string;
    Sabatino: string;
    Inversión: string;
    urlPrograma: string;
}
