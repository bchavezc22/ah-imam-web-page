export interface ControlResponse {
    codeResult: string;
    descriptionError: string;
    traceError: string;
}
