import { NgModule } from '@angular/core';
import { SharedModule } from '../../app/shared/shared.module'

import { LicenciaturaRoutingModule } from './licenciatura-routing.module';
import { LicenciaturaComponent } from './licenciatura.component';


@NgModule({
  declarations: [LicenciaturaComponent],
  imports: [
    SharedModule,
    LicenciaturaRoutingModule
  ]
})
export class LicenciaturaModule { }
