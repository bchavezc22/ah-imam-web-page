import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicenciaturaComponent } from './licenciatura.component';

const routes: Routes = [{ path: '', component: LicenciaturaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LicenciaturaRoutingModule { }
