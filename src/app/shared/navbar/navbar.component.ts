import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  Yposition: number= 0;
  isMobile: boolean = false;

  @HostListener('window:scroll', ['$event']) onScroll(e: Event){
    this.Yposition = this.getYPosition(e)
  }

  @HostListener('window:resize', ['$event']) onResize(event: Event) {
    this.Yposition = this.getYWindowPosition(event)
  }

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  getYPosition(e: Event): number {
    return (e.target as Document).scrollingElement.scrollTop;
  }

  getYWindowPosition(windowEvent: Event): number{
    return (windowEvent.target as Window).scrollY
  }

  applyScrollCssClass(typeEvent: string): boolean{
    var canAply = false;

    if( typeEvent === 'default'){
      if(  window.innerWidth>=992 ){
        canAply = true;
      }
    }else if( typeEvent === 'scroll' ){
      if( this.Yposition>100 && window.innerWidth>=992){
        canAply = true
      }
    }else if( typeEvent === 'mobile' ){
      if( window.innerWidth<=992 ){
        canAply = true
        this.isMobile = true
      }
    }

    return canAply;
  }
}
