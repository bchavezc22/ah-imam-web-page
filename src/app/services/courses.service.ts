import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import jsonDataCursos from '../../assets/jsons/cursos.json'
import { Curso } from '../interfaces/curso';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {


  private courseToPass: Curso = {
    id: "",
    descripcionCurso: "",
    tituloCurso: "",
    urlPrograma: ""
  }

  private coursesListToContact = new BehaviorSubject<Curso>(this.courseToPass)

  currentDataCourse = this.coursesListToContact.asObservable()

  constructor() { }

  
  public obtainCourses(): Array<Curso>{
    let  coursesList: Array<Curso>
    coursesList = new Array<Curso>()
    coursesList = jsonDataCursos.cursos;
    return coursesList;
  }

  newDataSelectedCourse( dataToPass: Curso){
    this.coursesListToContact.next(dataToPass)
  }

}
