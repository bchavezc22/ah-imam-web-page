import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ContactResponse } from '../interfaces/contact-response';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private isRegistered : boolean = false

  constructor(private httpServiceClient: HttpClient) { }

  handleError(error: HttpErrorResponse){
    let errorMessage = 'Unknow error'
    if(error.error instanceof ErrorEvent){
      errorMessage = `Error: ${error.error.message}`
    }else{
      errorMessage = `Error code: ${error.status}\nMessage: ${error.message}`
    }
    return throwError(errorMessage)
  }

  public isUserRegistered() : boolean {
    return this.isRegistered
  }

  public setRegister(){
    this.isRegistered = true
  }


  public postRequest(methodName: string, bodyRequest: any): Observable<ContactResponse>{
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
    }

    return this.httpServiceClient
    .post<ContactResponse>('https://5sxci7grfd.execute-api.us-east-2.amazonaws.com/sendContactIMAM', bodyRequest ? bodyRequest: {}, httpOptions)
    .pipe(catchError(this.handleError))
  }

}
