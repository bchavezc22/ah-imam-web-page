import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Grade } from '../interfaces/grade';
import jsonDataDiplomados from '../../assets/jsons/diplomados.json'

@Injectable({
  providedIn: 'root'
})
export class GradesService {


  private gradeToPass: Grade = {
     id:"",
     tituloCurso:"",
     descripcionCurso:"",
     Duración: "",
     HECSE: "",
     Inversión: "",
     Modalidad: "",
     Sabatino: "",
     urlPrograma: ""
  };

  private gradeList = new BehaviorSubject<Grade>(this.gradeToPass);

  currentDataGrade = this.gradeList.asObservable()

  constructor() { }

  obtainGradesList(): Array<Grade>{
    let listgrades: Array<Grade>;
    listgrades = new Array<Grade>()
    listgrades = jsonDataDiplomados.cursos
    return listgrades;
  }

  newSelectedGrade(selected: Grade){
    this.gradeList.next(selected)
  }

}
