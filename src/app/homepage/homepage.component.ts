import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { GoogleTagManagerService } from 'angular-google-tag-manager';

declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})

export class HomepageComponent implements OnInit {
  
  
  constructor(
    private gtmService: GoogleTagManagerService,
    private router: Router,
    private titleService: Title
    ) { }

  ngOnInit(): void {
    this.gtmService.addGtmToDom();
    this.setTitleDocument("IMAM")
  }

  private setTitleDocument( nameTab: string ){
    this.titleService.setTitle(nameTab)
  }

  downloadHomeInfo(urlDownload: string, pdfName:string){
    FileSaver.saveAs(urlDownload, pdfName);
  }

}
