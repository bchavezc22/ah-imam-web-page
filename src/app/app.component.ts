import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'actuary-hunters';

  constructor(
    private router: Router
    ) { 
      const navEndEvents = this.router.events.pipe(
        filter( event=> event instanceof NavigationEnd)
      )
    
      navEndEvents.subscribe((eventValue: NavigationEnd)=>{
        gtag('config', 'UA-135356550-1', {
          'page_path': eventValue.urlAfterRedirects
        });
      })
    }
}
