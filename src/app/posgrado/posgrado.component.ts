import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-posgrado',
  templateUrl: './posgrado.component.html',
  styleUrls: ['./posgrado.component.scss']
})
export class PosgradoComponent implements OnInit {

  constructor(
    private router: Router,
    private gtmService: GoogleTagManagerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.gtmService.addGtmToDom();
    this.setTitleDocument("POSGRADO")
  }

  private setTitleDocument( nameTab: string ){
    this.titleService.setTitle(nameTab)
  }

}
