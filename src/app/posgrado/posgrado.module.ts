import { NgModule } from '@angular/core';
import { SharedModule } from '../../app/shared/shared.module';

import { PosgradoRoutingModule } from './posgrado-routing.module';
import { PosgradoComponent } from './posgrado.component';


@NgModule({
  declarations: [PosgradoComponent],
  imports: [
    SharedModule,
    PosgradoRoutingModule
  ]
})
export class PosgradoModule { }
