import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PosgradoComponent } from './posgrado.component';

const routes: Routes = [{ path: '', component: PosgradoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PosgradoRoutingModule { }
