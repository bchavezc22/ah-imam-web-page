import { NgModule } from '@angular/core';
import { SharedModule } from '../../app/shared/shared.module';

import { ThanksRoutingModule } from './thanks-routing.module';
import { ThanksComponent } from './thanks.component';


@NgModule({
  declarations: [ThanksComponent],
  imports: [
    SharedModule,
    ThanksRoutingModule
  ]
})
export class ThanksModule { }
