import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss']
})
export class ThanksComponent implements OnInit {

  constructor(
    private router: Router,
    private gtmService: GoogleTagManagerService,
    private titleService: Title
    ) { }

  ngOnInit(): void {
    this.gtmService.addGtmToDom();
    
    this.setTitleDocument("THANK YOU")
   }

  private setTitleDocument( nameTab: string ){
    this.titleService.setTitle(nameTab)
  }
  

}
