import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardGuard } from './guard/authguard.guard';
import { HomepageComponent } from './homepage/homepage.component';


const routes: Routes = [
  { path: "home", component: HomepageComponent },
  { path: "", component: HomepageComponent },
  //{ path: "**", component: HomepageComponent },
  { path: 'contacto', loadChildren: () => import('./contacto/contacto.module').then(m => m.ContactoModule) },
  { path: 'licenciatura', loadChildren: () => import('./licenciatura/licenciatura.module').then(m => m.LicenciaturaModule) },
  { path: 'diplomados', loadChildren: () => import('./diplomados/diplomados.module').then(m => m.DiplomadosModule) },
  { path: 'posgrado', loadChildren: () => import('./posgrado/posgrado.module').then(m => m.PosgradoModule) },
  { path: 'nosotros', loadChildren: () => import('./aboutus/aboutus.module').then(m => m.AboutusModule) },
  { path: 'thankyou', canLoad: [AuthguardGuard], loadChildren: () => import('./thanks/thanks.module').then(m => m.ThanksModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
