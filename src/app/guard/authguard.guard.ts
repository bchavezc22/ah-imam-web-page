import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { ContactService } from '../services/contact.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanLoad {

  constructor( private contactService : ContactService){

  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
    
    if( this.contactService.isUserRegistered ){
      return true
    }
    
    return false
  }
 
  
}
