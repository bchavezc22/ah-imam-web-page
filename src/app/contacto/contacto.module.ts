import { NgModule } from '@angular/core';

import { ContactoRoutingModule } from './contacto-routing.module';
import { ContactoComponent } from './contacto.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { OnlynumbersDirective } from '../directives/onlynumbers.directive'


@NgModule({
  declarations: [ContactoComponent, OnlynumbersDirective],
  imports: [
    SharedModule,
    ContactoRoutingModule,
    FormsModule,
    HttpClientModule
  ]
})
export class ContactoModule { }
