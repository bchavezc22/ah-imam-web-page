import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ContactResponse } from '../interfaces/contact-response';
import { Service } from '../interfaces/service';
import { ContactService } from '../services/contact.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {


  servicesList: Array<Service>
  selectService: string = ""
  nameContact: string 
  mailContact: string 
  phoneContact: string 
  messageContact: string =""
  destroy$: Subject<boolean> = new Subject<boolean>()
  

  constructor( 
    private router: Router,
    private gtmService: GoogleTagManagerService,
    private serviceContactData: ContactService,
    private toastr: ToastrService,
    private titleService: Title
   ) { }

   ngOnInit(): void {
     this.servicesList =  [
        {
          id: "s1",
          tituloServicio: "Licenciatura"
        },
        {
        id: "s2",
        tituloServicio: "Diplomados"
        },
        {
        id: "s3",
        tituloServicio: "Posgrados"
        }
     ]
    this.gtmService.addGtmToDom();

    this.setTitleDocument("CONTACTO")
   }

  private setTitleDocument( nameTab: string ){
    this.titleService.setTitle(nameTab)
  }


  onSubmitContactInfo(formSubmit: NgForm){
  
    if( this.validFormValues(formSubmit) ){
      let contactPayload = {
        contentContact: {
          name: this.nameContact,
          email: this.mailContact,
          phoneNumber: this.phoneContact,
          carrer: "",
          service: "IMAM",
          userMessage: this.messageContact
        }
      }
      this.sendInfoContact(contactPayload, formSubmit)
    }else{
      this.formControlsValidators(formSubmit)
      
      this.toastr.error('Los datos marcados son necesarios para contactarnos contigo',  '¡Ups!...',{
        timeOut: 2000,
      })
    }
  }

  private validFormValues(form: NgForm): boolean{
    let canSendInfo = false;

    if( (this.nameContact!=null && this.nameContact!="") && (this.mailContact!=null && this.mailContact!="") && (this.phoneContact!=null && this.phoneContact!="") ){    
      if( form.controls['mailContactForm'].valid ){
        canSendInfo = true
      }
    }

    

    return canSendInfo;
  }

  private formControlsValidators( form: NgForm ){
      console.log(form)
      if(form.controls['contactNameForm'].untouched){
        form.controls['contactNameForm'].markAsTouched()
      }
      if(form.controls['mailContactForm'].untouched){
        form.controls['mailContactForm'].markAsTouched()
      }
      if(form.controls['phoneContactForm'].untouched){
        form.controls['phoneContactForm'].markAsTouched()
      }
  }

  private sendInfoContact(payload: any, form: NgForm){

    this.serviceContactData
     .postRequest('', payload)
     .pipe(takeUntil(this.destroy$))
     .subscribe((responseContact: ContactResponse)=>{
       if(responseContact.control.codeResult=='200'){
         this.serviceContactData.setRegister()
         this.router.navigate(["/thankyou"])
       }else{
         this.toastr.warning(responseContact.control.descriptionError, 'Aviso', {
           timeOut: 2000
         })
       }
    })
    
    form.reset()
  }
}
