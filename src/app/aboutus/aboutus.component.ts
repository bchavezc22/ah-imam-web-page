import { Component, OnInit, HostListener } from '@angular/core';
import { transition, trigger, useAnimation } from '@angular/animations';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { slideFadeIn, slideFadeOut, useSlideFadeInAnimation, useSlideFadeOutAnimation } from '../shared/animations/animations';
import { Title } from '@angular/platform-browser';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { bounceInAndOut, enterAndLeaveFromLeft, enterAndLeaveFromRight, fadeInAndOut,
  fadeInThenOut, growInShrinkOut, swingInAndOut } from '../shared/animations/triggers';
@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss'],
  animations: [
    // The following are pre-built triggers - Use these to add animations with the least work
    growInShrinkOut, fadeInThenOut, swingInAndOut, fadeInAndOut,
    enterAndLeaveFromLeft, enterAndLeaveFromRight, bounceInAndOut,

    // The following is a custom trigger using animations from the package
    // Use this approach if you need to customize the animation or use your own states
    trigger('enterFromLeftLeaveToRight', [
      // this transition uses a function that returns an animation with custom parameters
      transition(':enter', useSlideFadeInAnimation('10000ms', '20px')),
      // This transition uses useAnimation and passes the parameters directly - accomplishing the same thing as the above function
      transition(':leave', useAnimation(slideFadeOut, {params: {time: '2000ms', endPos: '100px'}})),
    ]),
  ]
})
export class AboutusComponent implements OnInit {


  
  show: boolean = false;

  @HostListener('window:scroll', ['$event']) onScroll(e: Event){
     if(this.getYPosition(e)>800){
        this.show = true
     }
  }
  
  constructor(
    private gtmService: GoogleTagManagerService,
    private router: Router,
    private titleService: Title
    ) { }

  ngOnInit(): void {
    this.gtmService.addGtmToDom();
   
    this.setTitleDocument("NOSOTROS")
  }

  private setTitleDocument( nameTab: string ){
    this.titleService.setTitle(nameTab)
  }

  getYPosition(e: Event): number {
    return (e.target as Document).scrollingElement.scrollTop;
  }


}
