import { NgModule } from '@angular/core';
import { SharedModule } from '../../app/shared/shared.module';

import { DiplomadosRoutingModule } from './diplomados-routing.module';
import { DiplomadosComponent } from './diplomados.component';


@NgModule({
  declarations: [DiplomadosComponent],
  imports: [
    SharedModule,
    DiplomadosRoutingModule
  ]
})
export class DiplomadosModule { }
