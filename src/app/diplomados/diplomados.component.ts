import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Title } from '@angular/platform-browser';

declare var require: any
const FileSaver = require('file-saver')

@Component({
  selector: 'app-diplomados',
  templateUrl: './diplomados.component.html',
  styleUrls: ['./diplomados.component.scss']
})
export class DiplomadosComponent implements OnInit {

  constructor(
    private router: Router,
    private gtmService: GoogleTagManagerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.gtmService.addGtmToDom();
    this.setTitleDocument("DIPLOMADOS")
  }

  private setTitleDocument( nameTab: string ){
    this.titleService.setTitle(nameTab)
  }

  downloadPdf(pdfUrl: string, pdfName: string ) {
    //const pdfUrl = './assets/sample.pdf';
    //const pdfName = 'your_pdf_file';
    FileSaver.saveAs(pdfUrl, pdfName);
  }


  navigateToContact(idElementSelected: string){
    // let valueToSend = this.dataGradesList.find( grade=>grade.id === idElementSelected )
    // this.gradeService.newSelectedGrade(valueToSend)
    this.router.navigate(["/contacto"])
  }

}
