import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiplomadosComponent } from './diplomados.component';

const routes: Routes = [{ path: '', component: DiplomadosComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiplomadosRoutingModule { }
